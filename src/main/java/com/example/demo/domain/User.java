package com.example.demo.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@NoArgsConstructor
@Data
public class User {
    private Long id;
    private String name;
    private Date createDate;
}
